<?php


namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class IdflyModel extends ActiveRecord
{
    public function rules()
    {
        return [
            [['name', 'email', 'phone', 'post'], 'required'],
            ['email', 'email'],
            [['ip', 'referrer'], 'safe']
        ];
    }

    public static function tableName()
    {
        return 'members';
    }
}