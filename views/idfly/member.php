<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php if (yii::$app->getSession()->getFlash('newUser')) {
    echo "<h2>Thanks for register!</h2>";
} ?>
<h2>You:</h2>
<?= Html::tag('p', $userSelf->getAttributeLabel('name') . ': ' . $userSelf['name']); ?>
<?= html::tag('p', $userSelf->getAttributeLabel('email') . ': ' . $userSelf['email']); ?>
<?= Html::tag('p', $userSelf->getAttributeLabel('phone') . ': ' . $userSelf['phone']); ?>
<?= Html::tag('p', $userSelf->getAttributeLabel('post') . ': ' . $userSelf['post']); ?>
<h2>Friends:</h2>
<div id="friendsList">
    <?php foreach ($friends as $friend) { ?>
        <?= Html::tag('p', $userSelf->getAttributeLabel('name') . ': ' . $friend['name']); ?>
        <?= html::tag('p', $userSelf->getAttributeLabel('email') . ': ' . $friend['email']); ?>
        <?= Html::tag('p', $userSelf->getAttributeLabel('phone') . ': ' . $friend['phone']); ?>
        <?= Html::tag('p', $userSelf->getAttributeLabel('post') . ': ' . $friend['post']); ?>
        <hr>
    <?php } ?>
</div>