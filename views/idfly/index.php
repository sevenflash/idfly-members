<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
    <h2>You:</h2>
<?php $form = ActiveForm::begin(); ?>
<?= $form->field($userSelf, '[0]name'); ?>
<?= $form->field($userSelf, '[0]email'); ?>
<?= $form->field($userSelf, '[0]phone'); ?>
<?= $form->field($userSelf, '[0]post'); ?>
<?= Html::button('New friend', ['id' => 'addFriendButton']) ?>
    <h2>Friends:</h2>
    <div id="friendsList">
        <?php foreach ($friends as $number => $friend) { ?>
            <?= $form->field($friend, "[$number]name"); ?>
            <?= $form->field($friend, "[$number]email"); ?>
            <?= $form->field($friend, "[$number]phone"); ?>
            <?= $form->field($friend, "[$number]post"); ?>
        <?php } ?>
    </div>
<?= Html::submitButton('Submit!'); ?>
    <script type="text/html" id="friend-template">
        <?= $form->field($newMember, '[{{id}}]name'); ?>
        <?= $form->field($newMember, '[{{id}}]email'); ?>
        <?= $form->field($newMember, '[{{id}}]phone'); ?>
        <?= $form->field($newMember, '[{{id}}]post'); ?>
        <hr>
    </script>
    <script type="text/javascript">
        var newFriendID = <?= count($friends) + 1; ?>;
        var template = document.getElementById('friend-template').innerHTML;
        var re = /{{id}}/g;
        document.getElementById('addFriendButton').addEventListener('click', function () {
            var newFriend = template.replace(re, newFriendID);
            var newFriendNode = document.createElement('div');
            newFriendNode.innerHTML = newFriend;
            document.getElementById('friendsList').appendChild(newFriendNode);
            newFriendID++;
        });
    </script>
<?php ActiveForm::end(); ?>