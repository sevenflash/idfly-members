<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\IdflyModel;
use yii\helpers\StringHelper;

class IdflyController extends Controller
{
    public function actionIndex()
    {
        $modelName = StringHelper::basename(IdflyModel::className());
        $success = false;
        $requestPost = yii::$app->request->post();
        $userIP = ip2long(yii::$app->request->getUserIP());
        if (IdflyModel::findOne(['ip' => $userIP])) {
            $this->redirect(yii::$app->urlManager->createUrl("idfly/member"));
        }
        $newMember = new IdflyModel();
        $data = isset($requestPost[$modelName]) ? $requestPost[$modelName] : false;
        $friends = array();
        if ($data) {
            $success = true;
            $userSelfData = array_splice($data, 0, 1)[0];
            $userSelfData['ip'] = $userIP;
            $userSelf = new IdflyModel();
            $userSelf->load([$modelName => $userSelfData]);
            if (!$userSelf->validate()) {
                $success = false;
            }
            $membersCount = 1;
            foreach ($data as $item) {
                $friend = new IdflyModel();
                $item['referrer'] = $userIP;
                $friend->load([$modelName => $item]);
                if (!$friend->validate()) {
                    $success = false;
                }
                $friends[++$membersCount] = $friend;
            }
            if ($success) {
                $userSelf->save();
                array_map(function ($friend) {
                    $friend->save();
                }, $friends);
                yii::$app->getSession()->setFlash('newUser', true);
                return $this->redirect(yii::$app->urlManager->createUrl("idfly/member"));
            }
        } else {
            $userSelf = $newMember;
        }
        return $this->render('index',
            ['userSelf' => $userSelf, 'newMember' => $newMember, 'friends' => $friends]);
    }

    public function actionMember()
    {
        $userIP = ip2long(yii::$app->request->getUserIP());
        $userSelf = IdflyModel::findOne(['ip' => $userIP]);
        if ($userSelf) {
            $friends = IdflyModel::find()->where(array('referrer' => $userIP))->asArray()->all();
        } else {
            $this->redirect(yii::$app->urlManager->createUrl("idfly/index"));
        }
        return $this->render('member', ['userSelf' => $userSelf, 'friends' => $friends]);
    }
}