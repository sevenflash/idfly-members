Idfly Members Task (sevenflash)
============================

Установка
------------

1) Склонировать репозиторий:

```
#!bash

git clone git@bitbucket.org:sevenflash/idfly-members.git
```
2) Скачать зависимости через composer:
```
#!bash


composer update
```


2) Настроить подключение к базе данных в файле config/db.php.

3) Выполнить миграцию базы данных:

```
#!bash

php yii migrate/up
```


Запустить приложение: http://localhost/index.php?r=idfly/index